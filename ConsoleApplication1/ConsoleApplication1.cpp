// ConsoleApplication1.cpp : Defines the entry point for the console application.
//
#include <conio.h>
#include <math.h>
#include <chrono>

#include <Windows.h>

#include "Data Types.h"

using namespace datatypes;

template<class T>
LinkedNode<T> * mergeSort(LinkedNode<T> *pList);
template<class T>
LinkedNode<T> * mergeSortDiff(LinkedNode<T> *pList);
template<class T>
LinkedNode<T> * mergeLists(LinkedNode<T> *a, LinkedNode<T> *b);
int findPos(int*, int);

int main()
{
	int jay(1);
	int & jax = jay;
	int * ptr = &jax;

	datatypes::LinkedNode<int> lone(1);
	datatypes::LinkedNode<int> ltwo(lone);

	datatypes::DoubleLinkedNode<int> llone(1);
	datatypes::DoubleLinkedNode<int> lltwo(llone);
	*lltwo.Data() = 2;
	

 	printf("\n\nLinked List\n");


	datatypes::SimplexLinkedList<int> SLL;
	LinkedNode<int> * one, *two, *three;
	one = new LinkedNode<int>(1);
	two = new LinkedNode<int>(2);
	three = new LinkedNode<int>(3);

	SLL.Insert(one);
	SLL.Insert(two);
	SLL.Insert(one, three);
	SLL.Insert(two, three);







	
	for (datatypes::SimplexLinkedList<int>::iterator it = SLL.Begin(); it != SLL.End(); it++) {
		printf("%i",(*it).DataValue());
	}







	printf("\n\nHashTable");
	datatypes::HashTable<int, std::string> ht;// std::string > ht;
	/*ht.Insert(1, "Test");
	ht.Insert(1, "tesT");
	ht.Insert(24, "test2");
	ht.Insert(2, "Tester");*/
	//*(ht[1].LeftDataPtr()) = 1;
	//Pair<int, std::string> * p = ht.Remove(1);
	/*if(!ht.Empty(ht.Search(1)))
		printf("%s", ht.Search(1).RightData().data());*/
	ht.Insert(0, "A");
	ht.Insert(23, "B"); 
	ht.Insert(46, "C");
	ht.Remove(23);

	printf("%s\n", ht.at(46).RightData().data());

	//ht.Allocate(50);
	printf("%s\n", ht.at(46).RightData().data());

	//ht[1] = 2;
	ht.Remove(0);
	ht.Insert(0, "test");
	Pair<int, std::string> s = ht[0];
	if(ht.IsDeleted(0))
		printf("%s", ht[1].RightData().data());
 	printf("\n");
	//int * test = new int();
	//*test = 5;  
	//datatypes::Node<int> n(test);// (5);
	//datatypes::Node<int> ns(6);

	//datatypes::Node<int> nf(7);

	//datatypes::Pair<datatypes::Node<int>, datatypes::Node<int>> pair(n,ns);
	//datatypes::Pair<datatypes::Node<int>, datatypes::Node<int>> pair2(ns, nf);
	//pair.swap(pair2);
	////datatypes::Pair<int, int> pair(5, 6);
	//datatypes::Pair<datatypes::Node<int>, datatypes::Node<int>>::swap(&pair);
	//printf("%i", *pair.getLeft().getData());
	//printf("%i", *pair.getRight().getData());


	//datatypes::Node<int> node1(test);
	//datatypes::Node<int> node2(test);
	//datatypes::DoubleLinkedNode<int> dnode1(6);
	//datatypes::DoubleLinkedNode<int> dnode2(test);
	//dnode1.Right(&dnode2);
	//dnode1.Right(&dnode1);
	//datatypes::DoubleLinkedNode<int> dnode3(5, dnode1, dnode2);
	//datatypes::DoubleLinkedNode<int> dn4(test, &dnode1, dnode3);

	////n.m_data = new int(5);
	////datatypes::DoubleLinkedNode<int> dln(5);// = new datatypes::DoubleLinkedNode<int>(5);

	//printf("%i", dnode2 == dnode1);
	//printf("%i", *n.getData());
	//printf("%i", *ns.getData());
	//*ns.getData() = 7;
	//if(ns == nf) 
	//	printf("%i", *nf.getData());

	//printf("\n");
	


	/*datatypes::HashTable<int,int> map(20);
	map.insert(0, 2);
	map.insert(2, 2);
	map.allocate(25);*/

 	//printf("%i\n",map.hash(4));
	

	printf("\n\nLinked Nodes / Merge Sorting");
	int val = 100;
	int *arr = new int[val];
	int *arr2 = new int[val];
	arr[0] = -100;
	arr2[0] = -100;
	for (int i = 1; i < val; i++) {
		arr[i] = arr[i-1] + rand() % 5;
		arr2[i] = arr2[i - 1] + rand() % 5;
		//printf("%i\n", arr[i]);
	}

	LinkedNode<int> myList(5);
	LinkedNode<int> SecondList(5);
	LinkedNode<int> * cur = &myList;
	LinkedNode<int> * cur2 = &SecondList;
	LinkedNode<int> pos(NULL);
	for (int i = 1; i < val; i++) {
		//arr[i] = arr[i - 1] + rand() % 5;
		cur->Next(new LinkedNode<int>(arr[i]));
		cur2->Next(new LinkedNode<int>(0));
		if (rand() % 2 == 1) *cur->Next()->Data() *= -1;
		cur = cur->Next();
		*cur2->Next()->Data() = cur->DataValue();
		cur2 = cur2->Next();
		//printf("%i\n", arr[i]);
	}

	LinkedNode<int> *newList;
	//newList = mergeSortDiff(&myList);

	cur = &SecondList;
	int i = 0;
	while (cur != NULL && i < 10) {
		i++;
		printf("%i : %i\n", i, cur->DataValue());
		cur = cur->Next();
	}

	newList = mergeSort(&SecondList);

	cur = newList;
	i = 0;
	while (cur != NULL && i < 10) {
		i++;
		printf("%i : %i\n",i, cur->DataValue());
		cur = cur->Next();
	}

	findPos(arr2, arr2[10]);


	 _getch();

    return 0;
}

#define NUMLISTS 32

template<class T>
LinkedNode<T> * mergeSort(LinkedNode<T> *pList)
{
	auto start = std::chrono::steady_clock::now();

	LinkedNode<T> * aList[NUMLISTS];                 // array of pointers to lists
	LinkedNode<T> * pNode;
	LinkedNode<T> * pNext;
	int i;
	if (pList == NULL)                   // check for empty list
		return NULL;
	for (i = 0; i < NUMLISTS; i++)       // zero array
		aList[i] = NULL;
	pNode = pList;
	i = 0; // merge nodes into array
	while (pNode != NULL) {
 		pNext = pNode->Next();
		(*pNode).Next(NULL);
		for (i = 0; (i < NUMLISTS) && (aList[i] != NULL); i++) {
			pNode = mergeLists(aList[i], pNode);
			aList[i] = NULL;
		}
		if (i == NUMLISTS)
			i--;
		aList[i] = pNode;
		pNode = pNext;
	}
	printf("I VAL : %i\n", i);
	pNode = NULL;                       // merge array into one list
	for (i = 0; i < NUMLISTS; i++)
		pNode = mergeLists(aList[i], pNode);
	printf("Time Taken : %i\n", std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count());
	return pNode;
}


template <class T>
LinkedNode<T> * mergeLists(LinkedNode<T> *a, LinkedNode<T> *b)
{
	LinkedNode<T> *pMerge = NULL;                    // ptr to merged list
	LinkedNode<T> **ppMerge = &pMerge;               // ptr to pMerge or prev->next
	if (a == NULL)
		return b;
	if (b == NULL)
		return a;

	while (1) {
		if (a->DataValue() <= b->DataValue()) {         // if a <= b
			*ppMerge = a;
			a = *(ppMerge = a->NextPtr());
			if (a == NULL) {
				*ppMerge = b;
				break;
			}
		}
		else {                        // b <= a
			*ppMerge = b;
			b = *(ppMerge = b->NextPtr());
			if (b == NULL) {
				*ppMerge = a;
				break;
			}
		}
	}
	return pMerge;
}

int findPos(int* arr, int key) {
	int sample = 10;
	int i = 0;
	int low=0, high=sample-1;
	bool hitBounds = false;

	while (arr[low] > key || arr[high] < key) {
		i = low + (((key - arr[low]) * (high - low)) / (arr[high] - arr[low]));
		low = i - (sample / 2);
		high = low + sample;
	}
	printf("My Key : %i\n", key);
	printf("Smaller List\n");
	for (int i = low; i < high; i++) {
		printf("item %i : val %i\n", i, arr[i]);
	}

	return 0;
}

