#ifndef DT_NODE_H
#define DT_NODE_H

#include <stdio.h>
#include <memory>
namespace datatypes {
	template <class T, class alloc_type = std::allocator<T>>
	
	struct RawNode {
	public:
		RawNode<T, alloc_type>() {
			m_data = NULL;
		}
		RawNode<T, alloc_type>(T *p_data) {
			m_data = p_data; // copy the location of the data not the data
		}
		RawNode<T, alloc_type>(T &p_data) {
			m_data = &p_data;
		}
		// Copying data from a const pointer seems like a bad idea
		// RawNode<T, alloc_type>(const T *p_data) { m_data = new T(*p_data); } // Clever hack but still a hack
		RawNode<T, alloc_type>(const T &p_data) {
			m_data = new T(p_data);
		}
		RawNode<T, alloc_type>(const RawNode<T> &p_n) : RawNode<T, alloc_type>(p_n.m_data) {}
		const bool operator== (const RawNode &p_rhs) const { return Compare_(p_rhs.m_data); };
		T * Data() { return m_data; }
		const T * Data() const { return m_data; }
		T &DataValue() { return *m_data; }
		const T &DataValue() const { return *m_data; }
	protected:
		virtual const bool Compare_(T * t) const { return m_data == t; }; // checking if pointing to same location (not if data is the same)
		virtual const bool Compare_(const T &t) const { return *m_data == t; }
		const bool Compare_(const RawNode &p_rhs) const { return Compare_(p_rhs.m_data); }
		T * m_data;
	};

	template <typename T>
	struct Node : public RawNode<T> {
	public:
		Node<T>() : RawNode<T>() {}
		Node<T>(Node<T> &p_n) : RawNode(p_n.Data()) {}
		Node<T>(T *p_data) : RawNode<T>(p_data) {}
		// Copying data from a const pointer seems like a bad idea
		// Node<T>(const T *p_data) : RawNode<T>(p_data) {} // Clever hack, but still a hack
		Node<T>(T &p_data) : RawNode<T>(p_data) {}
		Node<T>(const T &p_data) : RawNode<T>(p_data) {}
		

		const bool operator==(const Node<T> &p_rhs) const {
			return Compare_(*p_rhs.Data());
		}
		const bool operator==(Node<T> &p_rhs) const {
			return Compare_(p_rhs.Data());
		}
	private:
		const bool Compare_(Node<T> n)  const { return Compare_(n.Data()); }
		virtual const bool Compare_(T* t) const { return this->m_data == t; };
		virtual const bool Compare_(const T &t) const { return *this->m_data == t; }
	};

	template <typename P, typename P2>
	class Pair {
	public:
		Pair<P, P2>() : m_a_(), m_b_ () {}
		Pair<P, P2>(const P &p_lhs) {
			m_a_ = p_lhs;
			m_b_ = P2();
		}
		Pair<P, P2>(const P &p_lhs, const P2 &p_rhs) {
			m_a_ = p_lhs;
			m_b_ = p_rhs;
		}
		Pair<P, P2>(const Pair<P, P2> &p_pair) {
			m_a_ = p_pair.m_a_;
			m_b_ = p_pair.m_b_;
		}
		// swap?
		static void swap(Pair<P, P2> p_lhs, Pair<P, P2> p_rhs) {
			Node<P> n; Node<P2> n2;
			n = p_lhs.Left(); n2 = p_lhs.Right();
			p_lhs.m_a_ = p_rhs.m_a_;
			p_lhs.m_b_ = p_rhs.m_b_;
			p_rhs.m_a_ = n;
			p_rhs.m_b_ = n2;
		}

		Node<P> * Left() { return &m_a_; }
		const Node<P> * Left() const { return &m_a_; }
		P * LeftDataPtr() { return m_a_.Data(); }
		const P * LeftDataPtr() const { return m_a_.Data(); }
		P &LeftData() { return m_a_.DataValue(); }
		const P &LeftData() const { return m_a_.Data(); }

		Node<P2> * Right() { return &m_b_; }
		const Node<P2> * Right() const { return &m_b_; }
		P2 * RightDataPtr() { return m_b_.Data(); }
		const P2 * RightDataPtr() const { return m_b_.Data(); }
		P2 &RightData() { return m_b_.DataValue(); }
		const P2 &RightData() const { return m_b_.DataValue(); }
		
		bool operator== (Pair<P, P2> &p_rhs) {
			return ((m_a_ == p_rhs.m_a_) && (m_b_ == p_rhs.m_b_));
		}
		const bool operator==(const Pair<P, P2> &p_rhs) const {
			return ((m_a_ == p_rhs.m_a_) && (m_b_ == p_rhs.m_b_));
		}
	private:

		const P2 operator[](P iter) {
			return NULL; // implement later for maps
		}

		Node<P> m_a_;
		Node<P2> m_b_;
	};

	template <typename T>
	class LinkedNode : public Node<T> {
	public:
		LinkedNode<T>(T *p_data) : Node<T>(p_data) { Initialize_(nullptr); }
		LinkedNode<T>(T &p_data) : Node<T>(p_data) { Initialize_(nullptr); }
		LinkedNode<T>(const T &p_data) : Node<T>(p_data) { Initialize_(nullptr); }
		LinkedNode<T>(T *p_data, LinkedNode<T> *p_next) : LinkedNode<T>(p_data) { Initialize_(p_next); }
		LinkedNode<T>(T *p_data, LinkedNode<T> &p_next) : LinkedNode<T>(p_data) { Initialize_(&p_next); }
		LinkedNode<T>(T &p_data, LinkedNode<T> *p_next) : LinkedNode<T>(p_data) { Initialize_(p_next); }
		LinkedNode<T>(T &p_data, LinkedNode<T> &p_next) : LinkedNode<T>(p_data) { Initialize_(&p_next); }
		LinkedNode<T>(const T &p_data, LinkedNode<T> *p_next) : LinkedNode<T>(p_data) { Initialize_(p_next); }
		LinkedNode<T>(const T &p_data, LinkedNode<T> &p_next) : LinkedNode<T>(p_data) { Initialize_(&p_next); }
		LinkedNode<T>(LinkedNode<T> &p_ln) : LinkedNode<T>(p_ln.Data(),p_ln.next_) {} // copy constructor

		// Copying data from a const pointer seems like a bad idea
		//LinkedNode<T>(const T *p_data) : Node<T>(p_data) { Initialize_(nullptr); } // Clever hack but still a hack
		//LinkedNode<T>(T *p_data, const LinkedNode<T> *p_next) : LinkedNode<T>(p_data) { Initialize_(p_next); }
		//LinkedNode<T>(const T *p_data, LinkedNode<T> *p_next) : LinkedNode<T>(p_data) { Initialize_(p_next); }
		//LinkedNode<T>(const T *p_data, const LinkedNode<T> *p_next) : LinkedNode<T>(p_data) { Initialize_(p_next); }
		//LinkedNode<T>(const T *p_data, LinkedNode<T> &p_next) : LinkedNode<T>(p_data) { Initialize_(&p_next); }
		//LinkedNode<T>(const T &p_data, const LinkedNode<T> *p_next) : LinkedNode<T>(p_data) { Initialize_(p_next); }

		LinkedNode<T> * Next() { return next_; }
		const LinkedNode<T> * Next() const { return next_; }
		LinkedNode<T> ** NextPtr() { return &next_;	}
		// Set next pointer to a new location
		void Next(LinkedNode<T> * p_next) { next_ = p_next;	}

		bool operator==(LinkedNode<T> &p_ln) { return compare(p_ln); }
		const bool operator==(LinkedNode<T> &p_ln) const { return compare(p_ln); }
	protected:
		LinkedNode<T>() { Initialize_(); }
		LinkedNode<T> * next_ = nullptr;
	private:
		
		virtual void Initialize_() {
			Initialize_(nullptr);
		}
		virtual void Initialize_(LinkedNode<T>* p_next) {
			if (p_next == nullptr) {
				next_ = nullptr;
			}
			else {
				next_ = p_next;
			}
		}
		
		void Next(const LinkedNode<T> &p_next) {
			next_ = new LinkedNode<T>(p_next);
		}

		// only used internally
		const bool Compare_(LinkedNode<T> p_ln) const { // compare a full node to another
			return compare(p_ln.next_) && compare(p_ln.getData());
		}
		const bool Compare_(LinkedNode<T> *p_ln) const { // compare the node -> next_ pointer locations
			return lnext_ == p_ln;
		}
		virtual const bool Compare_(T* p_d) const { // compare the node pointer locations
			return this->m_data == p_d;
		}
		virtual const bool Compare_(const T &p_d) const {
			return *this->m_data == p_d;
		}
	};

	template <typename T>
	class DoubleLinkedNode  : public Node<T>{
	public:
		
		DoubleLinkedNode<T>(T *p_data) : Node<T>(p_data) {}
		DoubleLinkedNode<T>(T &p_data) : Node<T>(p_data) {}
		DoubleLinkedNode<T>(const T &p_data) : Node<T>(p_data) {}
		
		DoubleLinkedNode<T>(const T &p_data, DoubleLinkedNode<T> *p_left, DoubleLinkedNode<T> *p_right) : DoubleLinkedNode<T>(p_data) { Initialize_(p_left, p_right); }
		DoubleLinkedNode<T>(const T &p_data, DoubleLinkedNode<T> *p_left, DoubleLinkedNode<T> &p_right) : DoubleLinkedNode<T>(p_data, p_left, &p_next) {}
		DoubleLinkedNode<T>(const T &p_data, DoubleLinkedNode<T> &p_left, DoubleLinkedNode<T> *p_right) : DoubleLinkedNode<T>(p_data, &p_left, p_next) {}
		DoubleLinkedNode<T>(const T &p_data, DoubleLinkedNode<T> &p_left, DoubleLinkedNode<T> &p_right) : DoubleLinkedNode<T>(p_data, &p_left, &p_right) {}

		DoubleLinkedNode<T>(T *p_data, DoubleLinkedNode<T> *p_left, DoubleLinkedNode<T> *p_right) : DoubleLinkedNode<T>(p_data) { Initialize_(p_left, p_right); }
		DoubleLinkedNode<T>(T *p_data, DoubleLinkedNode<T> &p_left, DoubleLinkedNode<T> *p_right) : DoubleLinkedNode<T>(p_data, &p_left, p_right) {}
		DoubleLinkedNode<T>(T *p_data, DoubleLinkedNode<T> *p_left, DoubleLinkedNode<T> &p_right) : DoubleLinkedNode<T>(p_data, p_left, &p_right) {}
		DoubleLinkedNode<T>(T *p_data, DoubleLinkedNode<T> &p_left, DoubleLinkedNode<T> &p_right) : DoubleLinkedNode<T>(p_data, &p_left, &p_right) {}

		DoubleLinkedNode<T>(T &p_data, DoubleLinkedNode<T> *p_left, DoubleLinkedNode<T> *p_right) : DoubleLinkedNode<T>(p_data) { Initialize_(p_left, p_right); }
		DoubleLinkedNode<T>(T &p_data, DoubleLinkedNode<T> *p_left, DoubleLinkedNode<T> &p_right) : DoubleLinkedNode<T>(p_data, p_left, &p_next) {}
		DoubleLinkedNode<T>(T &p_data, DoubleLinkedNode<T> &p_left, DoubleLinkedNode<T> *p_right) : DoubleLinkedNode<T>(p_data, &p_left, p_next) {}
		DoubleLinkedNode<T>(T &p_data, DoubleLinkedNode<T> &p_left, DoubleLinkedNode<T> &p_right) : DoubleLinkedNode<T>(p_data, &p_left, &p_right) {}

		DoubleLinkedNode<T>(DoubleLinkedNode<T> &p_dln) : DoubleLinkedNode<T>(p_dln.Data(), p_dln.Left(), p_dln.Right()) {} // copy constructor
		// Copying data from a const pointer seems like a bad idea
		/*DoubleLinkedNode<T>(const T *p_data) : LinkedNode<T>(p_data) {}
		DoubleLinkedNode<T>(T *p_data, DoubleLinkedNode<T> *p_left, DoubleLinkedNode<T> *p_right) : DoubleLinkedNode<T>(p_data) { Initialize_(p_left, p_right); }
		DoubleLinkedNode<T>(T *p_data, DoubleLinkedNode<T> &p_left, DoubleLinkedNode<T> *p_right) : DoubleLinkedNode<T>(p_data, &p_left, p_right) {}
		DoubleLinkedNode<T>(T *p_data, DoubleLinkedNode<T> *p_left, DoubleLinkedNode<T> &p_right) : DoubleLinkedNode<T>(p_data, p_left, &p_right) {}
		DoubleLinkedNode<T>(T *p_data, DoubleLinkedNode<T> &p_left, DoubleLinkedNode<T> &p_right) : DoubleLinkedNode<T>(p_data, &p_left, &p_right) {}*/

		

		const bool operator== (DoubleLinkedNode<T> &p_dln) const {
			return compare(p_dln);
		}

		DoubleLinkedNode<T> * Left() { return left_; }
		DoubleLinkedNode<T> ** LeftPtr() { return &left_; }
		const DoubleLinkedNode<T> * Left() const { return left_; }
		const DoubleLinkedNode<T> ** LeftPtr() const { return &left_; }
		// Return true if left is a valid pointer
		// false if left is set to null
		bool Left(DoubleLinkedNode<T> *p_left) {
			left_ = p_left;
			return left_ != NULL;
		}
		
		DoubleLinkedNode<T> * Right() { return right_; }
		DoubleLinkedNode<T> ** RightPtr() { return &right_; }
		const DoubleLinkedNode<T> * Right() const { return right_; }
		const DoubleLinkedNode<T> ** RightPtr() const { return &right_; }
		bool Right(DoubleLinkedNode<T> *p_right) {
			right_ = p_right;
			return right_ != NULL;
		}
		
		bool ClearLeft() {
			left_ = NULL;
			return left_ == NULL;
		}
		bool ClearRight() {
			right_ = NULL;
			return right_ == NULL;
		}
		bool ClearLinks() {
			return ClearRight() && ClearLeft();
		}
	private:
		DoubleLinkedNode<T> * left_ = NULL;
		DoubleLinkedNode<T> * right_ = NULL;
		const bool Compare_(T *p_d) const {
			return this->m_data == p_d;
		}
		const bool Compare_(const T &p_d) const {
			return *this->m_data == p_d;
		}
		const bool Compare_(DoubleLinkedNode<T> p_dln) const {
			return compare(p_dln.left_, p_dln.right_) && compare(p_dln.getData());
		}
		const bool Compare_(DoubleLinkedNode<T> *p_left, DoubleLinkedNode<T> *p_right) const {
			return left_ == p_left && right_ == p_right;
		}

		void Initialize_() {
			Initialize_(NULL, NULL);
		}
		void Initialize_(DoubleLinkedNode<T> *p_left, DoubleLinkedNode<T> *p_next) {
			if (p_next == NULL) {
				right_ = NULL;
			}
			else {
				right_ = p_next;
			}
			if (p_left == NULL) {
				left_ = NULL;
			}
			else {
				left_ = p_left;
			}
		}
		DoubleLinkedNode<T>() { Initialize_(); }
	};
}

#endif

