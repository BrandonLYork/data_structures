#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#include <stdio.h>
#include <stdexcept>
#include <functional>

#include "../Node/Node.h"

const int cPRIME_ARRAY[] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199 };
const int cPRIME_ARRAY_SIZE = sizeof(cPRIME_ARRAY) / sizeof(int);

namespace datatypes {
	// K = key
	// V = value
	// C = comparator function
	// A = allocator function
	template <class K, class V, class M = std::modulus<K>>
	class HashTable {
	public:
		typedef K KeyType;
		typedef V ValueType;
		typedef M ModulusType;
		typedef Pair<K, V> PairType;

		HashTable() : data_(nullptr), size_(0), capacity_(20) {
			//Initialize_();
			Allocate_(NearestPrime(20));
		}
		HashTable(int p_size) : data_(nullptr), size_(0), capacity_(p_size) {
			//Initialize_();
			preventRehash_ = true;
			Allocate_(NearestPrime(p_size));
		}
		~HashTable() {}

		const int NearestPrime(const int &desired) {
			for (int i = 8; i < cPRIME_ARRAY_SIZE; i++) {
				if (cPRIME_ARRAY[i] >= desired) return cPRIME_ARRAY[i];
			}
			return desired;
		}

		virtual int Insert(const KeyType &p_key) {
			return Insert(p_key, V());
		}

		virtual int Insert(PairType p_pair) {
			return Insert(p_pair.LeftData(), p_pair.RightData());
		}

		virtual int Insert(const KeyType &p_key, const ValueType &p_val) {
			if (!preventRehash_) Rehash(); // *******************************
			int key = Hash(p_key);
			int key2 = Hash2(p_key);
			int index = -1;
			for (int i = 0; i < cMAX_PROBES_; i++) {
				index = (key + i * key2) % capacity_;
				if (Empty(data_[index])) {
					data_[index] = PairType(p_key, p_val);
					size_++;
					return index;
				}
				else if (data_[index].LeftData() == p_key) {
					*data_[index].RightDataPtr() = p_val;
					return index;
				}
			}
			return -1;
		}

		virtual PairType * Remove(const KeyType &p_key) {
			int hashedIndex = GetIndex_(p_key);
			PairType * pair = &data_[hashedIndex];
			data_[hashedIndex] = cDELETED_;
			size_--;
			if(!preventRehash_) Rehash(); // **********************
			return pair;
		}

		virtual PairType &Search(const KeyType &p_key) {
			return *GetPair_(p_key);
		}
		virtual const PairType &Search(const KeyType &p_key) const {
			return *GetPair_(p_key);
		}

		const bool IsDeleted(const int &p_index) const { return Deleted(data_[p_index]); }
		const bool Deleted(const KeyType p_key) const { return p_key == cDELETED_.LeftData(); }
		const bool Deleted(const PairType p_pair) const { return p_pair == cDELETED_; }

		const bool IsEmpty(const int &p_index) const { return Empty(data_[p_index]); }
		const bool Empty(const KeyType p_key) const { return p_key = cEMPTY_.LeftData(); }
		const bool Empty(const PairType p_pair) const { return p_pair == cEMPTY_; }


		const int Size() const { return size_; }

		// a = N / M 
		// a = load factor
		// N = Number of elements in array
		// M = total capacity of current array
		// if a > .5 rehash upwards - NewCap = M * 2
		// if a <= .1 rehash dowards - NewCap = N * 4
		// Each way assure that the new capacity places the current load factor around .25 allowing for equal movement upward or downward without needing a new rehash.
		void Rehash() {
			double a = size_ / capacity_;
			if (a > .5) {
				Allocate_(capacity_ * 2);
				return;
			}
			if (capacity_ > 23) {
				if (a <= .1) {
					Allocate_(size_ * 4);
					return;
				}
			}
			return;
		}

		virtual void SetBounds(const int &p_upper, const int &p_lower) {}
		virtual void DisableDownSize(bool p_trig = false) {}

		virtual int Hash(KeyType p_key, ModulusType m = ModulusType()) { // hash key into index
			return GetHVal_(p_key, m); // gets modulus size % p_key
		}
		virtual const int Hash(const KeyType &p_key, ModulusType m = ModulusType()) const {
			return GetHVal_(p_key, m); // gets modulus size % p_key
		}
		virtual int Hash2(KeyType p_key, ModulusType m = ModulusType()) {
			return GetH2Val_(p_key, m);
		}
		virtual const int Hash2(const KeyType &p_key, ModulusType m = ModulusType()) const {
			return GetH2Val_(p_key, m);
		}

		virtual PairType &at(const KeyType &p_key) {
			return Search(p_key);
		}
		virtual const PairType &at(const KeyType &p_key) const {
			return Search(p_key);
		}
		virtual PairType & operator [] (const KeyType &p_key) { // risky access type
																//return Search(p_key);
			if (p_key >= capacity_ || p_key < 0) throw std::out_of_range("Trying to access bad data : line 148");
			return data_[p_key];
		}
		virtual const PairType & operator[] (const KeyType &p_key) const {
			if (p_key >= capacity_ || p_key < 0) throw std::out_of_range("Trying to access bad data : line 152");
			return data_[p_key];
		}
	private:
		bool Allocate_(int p_newSize) {
			p_newSize = NearestPrime(p_newSize);
			if (data_ == nullptr) {
				data_ = new PairType[p_newSize];//
				for (int i = 0; i < p_newSize; i++) { data_[i] = PairType(cEMPTY_); }
				capacity_ = p_newSize;
				doubleHashCap_ = NearestPrime(capacity_ - (int)(.3f * capacity_));
				return true;
			}

			HashTable newTable(p_newSize);

			PairType empty = const_cast<PairType&>(cEMPTY_);
			PairType * cursor = &empty;
			for (unsigned i = 0, j = 0; i < size_; i++) {
				cursor = &empty;
				while ((Empty(*cursor) || Deleted(*cursor)) && j < capacity_) {
					cursor = &data_[j];
					j++;
					//printf("Checking %i", j);
				}
				newTable.Insert(cursor->LeftData(), cursor->RightData());
			}
			delete data_;
			data_ = newTable.data_;
			capacity_ = p_newSize;
			doubleHashCap_ = NearestPrime(capacity_ - (int)(.3f * capacity_));
			return true;
		}

		PairType * GetPair_(const KeyType &p_key) const {
			int key = Hash(p_key);
			int key2 = Hash2(p_key);
			int index = -1;
			for (int i = 0; i < cMAX_PROBES_; i++) {
				index = (key + i * key2) % capacity_;
				if (this->Empty(data_[index])) {
					break;
				}
				else if (!this->Deleted(data_[index])) {
					if (data_[index].LeftData() == p_key)
						break;
				}
			}
			return &data_[index];
		}

		const int GetIndex_(const KeyType &p_key) const {
			int key = Hash(p_key);
			int key2 = Hash2(p_key);
			int index = -1;
			for (int i = 0; i < cMAX_PROBES_; i++) {
				index = (key + i * key2) % capacity_;
				if (this->Empty(data_[index])) {
					return -1;
				}
				else if (!this->Deleted(data_[index])) {
					if (data_[index].LeftData() == p_key)
						return index;
				}
			}
			return -1;
		}

		const int GetHVal_(const KeyType &p_key, ModulusType m = ModulusType()) const {
			return m(p_key, capacity_);
		}
		const int GetH2Val_(const KeyType &p_key, ModulusType m = ModulusType()) const {
			// Double Hash Probing
			return (doubleHashCap_ - m(p_key, doubleHashCap_)) + 1; // prevents zero and one return values
																	// Quadratic Probing
																	//return (capacity_ - m(p_key, capacity_)) + 1;
																	// Linear Probing
																	//return 1;
		}


		bool preventRehash_ = false;
		bool needRehash_ = false;
		const int cMAX_PROBES_ = 10;

		const PairType cINVALID_ = PairType((K)-3);
		const PairType cDELETED_ = PairType((K)-2);
		const PairType cEMPTY_ = PairType((K)-1);

		float currentAlpha_; // a = N / M
		PairType * data_;
		std::size_t size_, capacity_, doubleHashCap_; // size = N, capacity = M
	};
}

#endif