#ifndef MY_LINKED_LIST_H
#define MY_LINKED_LIST_H

#include <memory>

namespace datatypes {
	template <typename T, typename L, typename alloc_type = std::allocator<T>>
	class LinkedList {
	public:	
		LinkedList() {}
		typedef T _dataType;
		typedef const T const_dataType;
		typedef L _linkedElement;
		typedef const L const_linkedElement;

		template <typename T, typename L>
		class const_iterator {
		public:
			const_iterator(L * p) : current(p) {}
			const L & operator* () const { return retrieve(); }; // indirection (requesting data from the pointer)
			const_iterator& operator++() { // pre-increment
				current = current->Right();
				return *this;
			}
			const_iterator operator++(int) { // post-increment
				const_iterator old = *this;
				++(*this);
				return old;
			}
			//const_iterator& operator--() { // pre-decrement
			//	current = current->Left();
			//}
			//const_iterator operator--(int) { // post-decrement
			//	const_iterator old = *this;
			//	--(*this);
			//	return old;
			//}
			bool operator== (const const_iterator &p_rhs) const {
				return current == p_rhs.current;
			}
			bool operator!= (const const_iterator &p_rhs) const {
				return current != p_rhs.current;
			}
		protected:
			const_iterator() : current(nullptr) {}
			L & retrieve() const { return current; }
			L * current;
			//friend class LinkedList<T,L>;
		};

		template <typename T, typename L>
		class iterator : const_iterator<T,L> {
		public:
			iterator(L * p) { this->current = p; }
			L& operator* () { return *this->current; }
			const L& operator* () const { return *this->current; }
			iterator& operator++() {
				this->current = this->current->Next();
				return *this;
			}
			iterator operator++(int) {
				iterator old = *this;
				++(*this);
				return old;
			}
			/*iterator& operator--() {
				this->current = this->current->Prev();
				return *this;
			}
			iterator operator--(int) {
				iterator old = *this;
				--(*this);
				return old;
			}*/
			bool operator== (const iterator &p_rhs) const {
				return this->current == p_rhs.current;
			}
			bool operator!= (const iterator &p_rhs) const {
				return this->current != p_rhs.current;
			}
		protected:
			iterator() {}
			//friend class LinkedList<T, L>;
		};

		template <typename T, typename L>
		class const_reverse_iterator : public const_iterator<T,L> {
		public:
			const_reverse_iterator(L * p) { current = p; }
			//const _linkedElement & operator * () const {}
			const_reverse_iterator& operator++() {}
			const_reverse_iterator operator++(int) {}
			const_reverse_iterator& operator--() {}
			const_reverse_iterator operator--(int) {}
		protected:
			const_reverse_iterator() {}
			
			//friend class LinkedList<T, L>;
			//friend class SimplexLinkedList<T, LinkedNode<T>>;
		};


		template <typename T, typename L>
		class reverse_iterator : public const_reverse_iterator<T,L> { // reverse -> const_reverse -> const
		public:
			reverse_iterator(L * p) { current = p; }
			//_linkedElement & operator* () {}
			//const _linkedElement & operator * () const {}
			reverse_iterator& operator++() {}
			reverse_iterator operator++(int) {}
			reverse_iterator& operator--() {}
			reverse_iterator operator--(int) {}
		protected:
			reverse_iterator() {}
			//friend class LinkedList<T, L>;
		};

		typedef const_iterator<T, L> _const_iterator;
		typedef iterator<T, L> _iterator;
		typedef const_reverse_iterator<T, L> _const_reverse_iterator;
		typedef reverse_iterator<T, L> _reverse_iterator;

		virtual void Assign(int, _dataType) = 0; // 
		virtual void Assign(int, const_dataType &) = 0;
		virtual alloc_type GetAllocator() const = 0;


		virtual _linkedElement * Insert(_dataType & p_val) = 0;
		virtual _linkedElement * Insert(const_dataType & p_val) = 0;
		virtual void Insert(_linkedElement * p_node) = 0;
		virtual void Insert(_linkedElement * p_lhs, _linkedElement * p_node) = 0;
		virtual void Insert(_linkedElement * p_lhs, _linkedElement * p_rhs, _linkedElement * p_node) = 0;
		//virtual const int & Insert() const = 0;

		virtual _linkedElement * Front() = 0;
		virtual const_linkedElement * Front() const = 0;
		virtual _linkedElement * Back() = 0;
		virtual const_linkedElement * Back() const = 0;

		virtual iterator<T,L> Begin() noexcept = 0;
		virtual _const_iterator Begin() const noexcept = 0; // possibly const
		virtual _const_iterator CBegin() const noexcept = 0; // always const

		virtual _iterator End() noexcept = 0;
		virtual _const_iterator End() const noexcept = 0;
		virtual _const_iterator CEnd() const noexcept = 0;

		// rbegin
		// crbegin
		// rend
		// crend

		virtual const bool Empty() const = 0;
		virtual const int Size() const = 0;
		//virtual const int max_size() const = 0; // Don't see the point
		virtual void Clear() = 0;
	};

	template <typename T, typename L = LinkedNode<T>, typename alloc_type = std::allocator<T>>
	class SimplexLinkedList : public LinkedList<T, L> {
	public:
		typedef T _dataType;
		typedef const T const_dataType;
		typedef L _linkedElement;
		typedef const L const_linkedElement;
		typedef LinkedList<T, L> _listType;
		typedef LinkedList<T, L>::const_iterator<T,L> const_iterator;
		typedef LinkedList<T, L>::iterator<T,L> iterator;
		//typedef LinkedList<T, L>::const_reverse_iterator<T,L> const_reverse_iterator;
		//typedef LinkedList<T, L>::reverse_iterator<T,L> reverse_iterator;

		SimplexLinkedList() { head_ = tail_ = nullptr; }


		virtual void Assign(int, _dataType) {}; // 
		virtual void Assign(int, const_dataType &) {};
		virtual alloc_type GetAllocator() const { return alloc_type(); };

		
		virtual _linkedElement * Insert(_dataType & p_val) {
			_linkedElement * elem = new _linkedElement(p_val);
			Insert(elem);
			return elem;
		}
		virtual _linkedElement * Insert(const_dataType & p_val) {
			_linkedElement * elem = new _linkedElement(p_val);
			Insert(elem);
			return elem;
		}

		virtual void Insert(_linkedElement * p_node) {
			if (head_ == nullptr) {
				head_ = tail_ = p_node;
				return;
			}
			*tail_->NextPtr() = p_node;
			
			_linkedElement * last = p_node->Next();
			tail_ = p_node;
			while (last != nullptr) {
				last = last->Next();
				tail_ = tail_->Next();
			}
		}
		virtual void Insert(_linkedElement * p_lhs, _linkedElement * p_node) {
			if (p_lhs == nullptr) return;
			return Insert(p_lhs, p_lhs->Next(), p_node);
		}
		virtual void Insert(_linkedElement * p_lhs, _linkedElement * p_rhs, _linkedElement * p_node) {
			if (p_lhs == nullptr) return;
			*p_lhs->NextPtr() = p_node;
			*p_node->NextPtr() = p_rhs;

			if (p_rhs == nullptr) {
				tail_ = p_node;
				return;
			}
			_linkedElement * last = p_rhs->Next();
			tail_ = p_rhs;
			while (last != nullptr) {
				last = last->Next();
				tail_ = tail_->Next();
			}
			return;
		} 

		virtual _linkedElement * Front() { return head_; };
		virtual const_linkedElement * Front() const { return head_; };
		virtual _linkedElement * Back() { return tail_; };
		virtual const_linkedElement * Back() const { return tail_; };

		virtual iterator Begin() noexcept { return iterator(head_); };
		virtual const_iterator Begin() const noexcept { return const_iterator(head_); }; // possibly const
		virtual const_iterator CBegin() const noexcept { return const_iterator(head_); }; // always const

		virtual iterator End() noexcept { return iterator(tail_->Next()); };
		virtual const_iterator End() const noexcept { return const_iterator(tail_->Next()); };
		virtual const_iterator CEnd() const noexcept { return const_iterator(tail_->Next()); };

		// rbegin
		// crbegin
		// rend
		// crend

		virtual const bool Empty() const { return false; };
		virtual const int Size() const { return 0; };
		//virtual const int max_size() const = 0; // Don't see the point
		virtual void Clear() { return; };
	protected:
		_linkedElement * head_;
		_linkedElement * tail_;
	};


	/*
	template <typename T>
	class DuplexLinkedList : public LinkedList {

	};*/
}

#endif